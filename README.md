# Ravel dokuwiki template

* Based on a wordpress theme
* Designed by [Justin Tadlock](https://wordpress.org/themes/ravel/)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:ravel)

![ravel theme screenshot](https://i.imgur.com/agH3Zob.png)